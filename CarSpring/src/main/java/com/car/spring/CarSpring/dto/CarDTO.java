package com.car.spring.CarSpring.dto;

public class CarDTO {
	private Long id;
	private String car_number;
	private String car_brand;
	public CarDTO() {
		super();
	}
	public CarDTO(Long id, String car_number, String car_brand) {
		super();
		this.id = id;
		this.car_number = car_number;
		this.car_brand = car_brand;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCar_number() {
		return car_number;
	}
	public void setCar_number(String car_number) {
		this.car_number = car_number;
	}
	public String getCar_brand() {
		return car_brand;
	}
	public void setCar_brand(String car_brand) {
		this.car_brand = car_brand;
	}
	@Override
	public String toString() {
		return "CarDTO [id=" + id + ", car_number=" + car_number + ", car_brand=" + car_brand + "]";
	}
}
