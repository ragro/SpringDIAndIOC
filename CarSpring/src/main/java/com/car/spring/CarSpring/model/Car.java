package com.car.spring.CarSpring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Car {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String car_number;
	private String car_brand;
	public Car() {
		super();
	}
	public Car(Long id, String car_number, String car_brand) {
		super();
		this.id = id;
		this.car_number = car_number;
		this.car_brand = car_brand;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCar_number() {
		return car_number;
	}
	public void setCar_number(String car_number) {
		this.car_number = car_number;
	}
	public String getCar_brand() {
		return car_brand;
	}
	public void setCar_brand(String car_brand) {
		this.car_brand = car_brand;
	}
	@Override
	public String toString() {
		return "Car [id=" + id + ", car_number=" + car_number + ", car_brand=" + car_brand + "]";
	}	
}
