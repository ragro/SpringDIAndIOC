package com.springPractice;

public class BaseballCoach implements Coach {

	//declare a private field for dependency
		private FortuneService fortuneService;
		
		//define constructor for dependency
		public BaseballCoach(FortuneService theFortuneService) {
			fortuneService = theFortuneService;
		}
		
	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Do 30 mins batting practice daily";
	}

	@Override
	public String getDailyFortune() {
		//use fortuneservice to get fortune 
		return fortuneService.getFortune();
	}
	
	//add an init method 
	public void domyStartupStuff() {
		System.out.println("\nBaseballCoach:inside method domyStartupStuff");
	}
	
	//add destro method
public void domyCleanupStuff() {
	System.out.println("\nBaseballCoach:inside method domyCleanupStuff");
	}

}
