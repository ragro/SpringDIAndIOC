package com.springPractice;

public interface Coach {
	String getDailyWorkout();
	String getDailyFortune();
}
