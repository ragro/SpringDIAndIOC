package com.springPractice;

public class CricketCoach implements Coach {

//add new fields for email and team	
	private String email;
	private String team;
// make private varibale for dependency
	private FortuneService fortuneService;
	
//	create a no-arg constructor
	public CricketCoach() {
		System.out.println("Hey I'm inside Cricket Coach");
	}
	
//	setter method for injection of dependency
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("CricketCoach: inside setter method - setFortuneService");
		this.fortuneService = fortuneService;
	}


	public String getEmailAddress() {
		return email;
	}

	public String getTeam() {
		return team;
	}

	public void setEmailAddress(String email) {
		System.out.println("CricketCoach: inside setter method - setEmail");
		this.email = email;
	}

	public void setTeam(String team) {
		System.out.println("CricketCoach: inside setter method - setTeam");
		this.team = team;
	}

	@Override
	public String getDailyWorkout() {
		return "practice fast bowling for 15 mins daily";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}

}
