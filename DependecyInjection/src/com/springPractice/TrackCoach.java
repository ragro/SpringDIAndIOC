package com.springPractice;

public class TrackCoach implements Coach {

	//declare a private field for dependency
	private FortuneService fortuneService;
	
	public TrackCoach() {
	}
	//define constructor for dependency
	public TrackCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Run 5km daily";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return null;
	}

}
