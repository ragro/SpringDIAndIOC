package com.spring.annotation;

public interface Coach {
	public String getDailyWorkOut();
	public String getDailyFortune();
}
