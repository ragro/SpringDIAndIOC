package com.spring.annotation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class CustomFortuneService implements FortuneService {

	private String fileName = "/home/rohit/eclipse-workspace/Spring-Demo-Annotations/src/fortune-data.txt";
	private List<String> fortuneList;
	private Random random = new Random();
	
	
	
	public CustomFortuneService() {
		fortuneList = new ArrayList<String>();
		File theFile = new File(fileName);
		
		//read file
		try(BufferedReader br = new BufferedReader(
				new FileReader(theFile))){
			String line; 
			
			while((line = br.readLine()) != null) {
				fortuneList.add(line);
			}
			
		}catch(IOException e) {
			System.out.println(e);
		}
		
	}
    
	@Override
	public String getFortune() {
		// TODO Auto-generated method stub
		int index = random.nextInt(fortuneList.size()) ;
		String todayFortune = fortuneList.get(index);
//		System.out.println(todayFortune);
		return todayFortune ;
	}

	@PostConstruct
	public void readFileAfterConstruct() {
		File theFile = new File(fileName);
		
		try(BufferedReader br = new BufferedReader(
				new FileReader(theFile))){
			String line;
			while((line = br.readLine()) != null) {
				System.out.println(line);
			}
			
		}catch(IOException e) {
			System.out.println(e);
		}
	}
}
