package com.spring.annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class JavaConfigDemoApp {

	public static void main(String[] args) {
		//load spring container
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
		//retrieve beans form container
		Coach theCoach = context.getBean("tennisCoach", Coach.class);
		//use methods of bean
		System.out.println(theCoach.getDailyWorkOut());
		System.out.println(theCoach.getDailyFortune());
		//close the context
		context.close();
	}

}
