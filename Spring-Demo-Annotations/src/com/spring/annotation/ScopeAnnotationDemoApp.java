package com.spring.annotation;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ScopeAnnotationDemoApp {

	public static void main(String[] args) {
		//load spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//retrieve beans from container
		Coach theCoach = context.getBean("tennisCoach", Coach.class);
		Coach otherCoach = context.getBean("tennisCoach", Coach.class);
		
		//use method of beans
		System.out.println(theCoach.getDailyWorkOut());
		
		//check for scope
		boolean result = (theCoach == otherCoach);
		System.out.println(result);
	
		//close context
		context.close();
	}

}
