package com.spring.annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringJavaConfigDemoApp {

	public static void main(String[] args) {
		//load Spring Configurations from Java Class 
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
		
		//retrieve beans from container
		Coach theCoach = context.getBean("swimCoach", Coach.class);
		//use methds
		System.out.println(theCoach.getDailyWorkOut());
		System.out.println(theCoach.getDailyFortune());
		//close context
		context.close();
	}

}
