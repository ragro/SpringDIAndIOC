package com.spring.annotation;

public class SwimCoach implements Coach {

	private FortuneService fortuneService;
	
	
	public SwimCoach(FortuneService theFortuneService) {
		super();
		this.fortuneService = theFortuneService;
	}

	@Override
	public String getDailyWorkOut() {
		return "Do swimming for 30 mins daily...";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}
