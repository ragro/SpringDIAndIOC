package com.spring.annotation;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("tennisCoach")
//@Scope("prototype")
public class TennisCoach implements Coach {
//  Field njection
//	@Autowired
//	@Qualifier("databaseFortuneService")
	private FortuneService fortuneService;
	
	
	
	public TennisCoach() {
		super();
		System.out.println("Hey inside TennisCoach : Defualt Constructor");
	}

//  Constructor Injection
//	@Autowired
//	public TennisCoach(@Qualifier("happyFortuneService")FortuneService fortuneService) {
//		super();
//		this.fortuneService = fortuneService;
//	}
	
//	setter injection
//	@Autowired
//	@Qualifier("randomFortuneService")
//	public void setFortuneService(FortuneService fortuneService) {
//		System.out.println("Tennis Coach : inside method : setFortuneService");
//		this.fortuneService = fortuneService;
//	}
//	
	//method injection
	@Autowired
	@Qualifier("customFortuneService")
	public void doSomeCrazyStuff(FortuneService fortuneService) {
		System.out.println("Tennis Coach : inside method : DoSomeCrazyStuff");
		this.fortuneService = fortuneService;
	}


	@Override
	public String getDailyWorkOut() {
		// TODO Auto-generated method stub
		return "do 30 min fast bowling pratice";
	}
	
	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
		
	}
	
	//make init method
	@PostConstruct
	public void doConstructStuff() {
		System.out.println("TennisCoach : inside method : doConstructStuff");
	}
	
	
	//make destroy method
	@PreDestroy
	public void doCleanUpStuff() {
		System.out.println("TennisCoach: inside method : doCleanUpStuff");
	}
	
}
