package com.example.demo.di.demoDI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.example.demo.di.demoDI.controller.DemoController;

@SpringBootApplication
public class DemoDiApplication {

	public static void main(String[] args) {
	 ApplicationContext ax = SpringApplication.run(DemoDiApplication.class, args);
	  
	 DemoController controller = (DemoController) ax.getBean("demoController"); 
	 controller.getHello();
	}
}
