package com.example.demo.di.demoDI.controller;

import com.example.demo.di.demoDI.services.GreetingService;

	public class ConstructorInjectedController {
	private GreetingService greetingService;
		
		public ConstructorInjectedController(GreetingService greetingService) {
			super();
			this.greetingService = greetingService;
		}

		public String sayHello() {
			return greetingService.sayGreeting();
		}
	
}
