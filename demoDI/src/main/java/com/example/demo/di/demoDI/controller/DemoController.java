package com.example.demo.di.demoDI.controller;

import org.springframework.stereotype.Controller;

@Controller
public class DemoController {
	
	public String getHello() {
		System.out.println("Hello....");
		return "Helloooo...";
	}
}
