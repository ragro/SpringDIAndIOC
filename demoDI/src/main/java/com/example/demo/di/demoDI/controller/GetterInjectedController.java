package com.example.demo.di.demoDI.controller;

import com.example.demo.di.demoDI.services.GreetingService;

public class GetterInjectedController {
		
	private GreetingService greetingService;
		
		public String sayHello() {
			return greetingService.sayGreeting();
		}
	
		public void setGreetingService(GreetingService greetingService) {
			this.greetingService = greetingService;
		}

}
