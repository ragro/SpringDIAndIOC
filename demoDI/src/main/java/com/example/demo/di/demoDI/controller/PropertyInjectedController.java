package com.example.demo.di.demoDI.controller;

import com.example.demo.di.demoDI.services.GreetingServiceImpl;

public class PropertyInjectedController {
	public GreetingServiceImpl greetingService;
	
//	
//	public PropertyInjectedController(GreetingServiceImpl greetingService) {
//		super();
//		this.greetingService = greetingService;
//	}

	public String sayHello() {
		return greetingService.sayGreeting();
	}
	
	public GreetingServiceImpl getGreetingService() {
		return greetingService;
	}
}
