package com.example.demo.di.demoDI.services;

public interface GreetingService {
	public String sayGreeting();
}
