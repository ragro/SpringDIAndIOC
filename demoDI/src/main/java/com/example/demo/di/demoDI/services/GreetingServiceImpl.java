package com.example.demo.di.demoDI.services;

import org.springframework.stereotype.Service;

@Service
public class GreetingServiceImpl implements GreetingService{

	public static final String Hello = "Hello Guru ji....";
	
	@Override
	public String sayGreeting() {
		return Hello;
	}
	
}
