package com.example.demo.di.demoDI.controllers;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.example.demo.di.demoDI.controller.GetterInjectedController;
import com.example.demo.di.demoDI.services.GreetingServiceImpl;

public class GetterInjectedControllerTest {
	private GetterInjectedController getterInjectedController;
	
	@Before
	public void setUp() throws Exception{
		this.getterInjectedController = new GetterInjectedController();
		this.getterInjectedController.setGreetingService(new GreetingServiceImpl());
	}
	
	@Test
	public void testGreeting() {
		assertEquals(GreetingServiceImpl.Hello , getterInjectedController.sayHello());
	}
}
