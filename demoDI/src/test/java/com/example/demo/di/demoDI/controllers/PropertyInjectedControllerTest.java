package com.example.demo.di.demoDI.controllers;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.example.demo.di.demoDI.controller.PropertyInjectedController;
import com.example.demo.di.demoDI.services.GreetingServiceImpl;

public class PropertyInjectedControllerTest {
	private PropertyInjectedController propertyInjectedController;

	@Before
	public void setUp() {
		this.propertyInjectedController = new PropertyInjectedController();
		this.propertyInjectedController.greetingService = new GreetingServiceImpl();
	}
	
	@Test
	public void testGreeting() {
		assertEquals(GreetingServiceImpl.Hello , propertyInjectedController.sayHello());
	}
}
