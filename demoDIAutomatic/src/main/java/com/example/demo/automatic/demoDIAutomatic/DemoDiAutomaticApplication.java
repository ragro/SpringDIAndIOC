package com.example.demo.automatic.demoDIAutomatic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.example.demo.automatic.demoDIAutomatic.controller.ConstructorInjectedController;
import com.example.demo.automatic.demoDIAutomatic.controller.DemoController;
import com.example.demo.automatic.demoDIAutomatic.controller.GetterInjectedController;
import com.example.demo.automatic.demoDIAutomatic.controller.PropertyInjectedController;



@SpringBootApplication
public class DemoDiAutomaticApplication {

	public static void main(String[] args) {
		ApplicationContext ax = SpringApplication.run(DemoDiAutomaticApplication.class, args);
		 DemoController controller = (DemoController) ax.getBean("demoController"); 
		 controller.getHello();
		 
		 System.out.println(ax.getBean(PropertyInjectedController.class).sayHello());
		 System.out.println(ax.getBean(GetterInjectedController.class).sayHello());
		 System.out.println(ax.getBean(ConstructorInjectedController.class).sayHello());
	}
}
