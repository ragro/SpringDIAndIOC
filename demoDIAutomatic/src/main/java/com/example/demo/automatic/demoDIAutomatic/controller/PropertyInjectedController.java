package com.example.demo.automatic.demoDIAutomatic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.example.demo.automatic.demoDIAutomatic.services.GreetingServiceImpl;


@Controller
public class PropertyInjectedController {
	
	@Autowired
	public GreetingServiceImpl greetingService;
	
//	
//	public PropertyInjectedController(GreetingServiceImpl greetingService) {
//		super();
//		this.greetingService = greetingService;
//	}

	public String sayHello() {
		return greetingService.sayGreeting();
	}
	
	public GreetingServiceImpl getGreetingService() {
		return greetingService;
	}
}
