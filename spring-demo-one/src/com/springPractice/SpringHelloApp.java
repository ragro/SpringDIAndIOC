package com.springPractice;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringHelloApp {

	public static void main(String[] args) {

		//		loading Spring Configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
			
//		retrieve beans from container
		Coach theCoach = context.getBean("myCoach", Coach.class);
		
//		call methods from beans
		
		System.out.println(theCoach.getDailyWorkout());
		
//		close context
		context.close();

	}

}
